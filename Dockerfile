FROM node:6.9.5
COPY ["package.json", "/usr/src"]
WORKDIR /usr/src
RUN npm run preinstall
EXPOSE 4200
CMD ["ng","serve","--host","0.0.0.0","--disable-host-check"]